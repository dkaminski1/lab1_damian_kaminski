const sql = require('mssql');
const parser = require('mssql-connection-string');

class MotionDbContext {
  constructor(connectionString, log) {
    log('MotionDbContext object has been created.');
    this.log = log;
    this.config = parser(connectionString);
    this.getMotion = this.getMotion.bind(this);
    this.addMotion = this.addMotion.bind(this);
  }

  async getMotion() {
    this.log('getMotion function - run');
    const connection = await new sql.ConnectionPool(this.config).connect();
    const request = new sql.Request(connection);
    const result = await request.query('select * from MotionMonitoring');
    this.log('getMotion function - done');

    return result.recordset;
  }

  async addMotion(detected) {
    this.log('addMotion function - run');
    const connection = await new sql.ConnectionPool(this.config).connect();
    const request = new sql.Request(connection);
    const result = await request.query(
      `insert into MotionMonitoring (detected) values (${detected})`
    );
    this.log('addMotion function - done');

    return result.recordset;
  }
}

module.exports = MotionDbContext;
