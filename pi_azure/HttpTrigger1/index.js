const MotionDbContext = require('../HttpTrigger1/DataAccess/db-context');
const common = require('./../common');

module.exports = async function(context, req) {
  await common.functionWrapper(context, req, async body => {
    const connectionString = process.env['motionDb'];
    const motionDb = new MotionDbContext(connectionString, context.log);
    body.motion = await motionDb.getMotion();
  });
};

//Data Source=tcp:cdv-dk.database.windows.net,1433;Initial Catalog=cdv_dkaminski1;User Id=cdv_dkaminski1;Password=1q2w3e4r5t!;

//Server=tcp:cdv-dk.database.windows.net,1433;Initial Catalog=pi_database;Persist Security Info=False;User ID={your_username};Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
