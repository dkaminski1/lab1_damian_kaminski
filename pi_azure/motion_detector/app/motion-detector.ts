import { Gpio } from 'onoff';

export default class MotionDetector {
  private interval: any; // delegate to interval function
  private led: Gpio; // LED Pin on raspberry PI
  private led2: Gpio;
  private motionDetector: Gpio; // Motion Detector Pin on raspberry PI
  private lastDetectorValue: number = 0; //Last motion detector value

  constructor(ledPin: number, ledPinSecond: number, motionDetectorPin: number) {
    this.led = new Gpio(ledPin, 'out');
    this.led2 = new Gpio(ledPinSecond, 'out');
    this.motionDetector = new Gpio(motionDetectorPin, 'in');

    //context binding
    this.run = this.run.bind(this);
    this.checkMotionDetectorState = this.checkMotionDetectorState.bind(this);
    this.turnOffLed = this.turnOffLed.bind(this);
  }

  run() {
    this.interval = setInterval(this.checkMotionDetectorState, 500);

    //the operation if user use ctrl + c
    process.on('SIGINT', this.turnOffLed);
  }

  private checkMotionDetectorState(): void {
    //get motion detector vaalue
    let value = this.motionDetector.readSync();

  }

  private turnOffLed(): void {
    console.log('-- Thank you for use Motion Detector app --');

    this.led.writeSync(0);
    this.led2.writeSync(0);

    //clear delegate to interval function
    clearInterval(this.interval);
  }
}